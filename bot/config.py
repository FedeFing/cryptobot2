import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import datetime
import time_utils
import enums


# Real
# tick_interval_s = 1
# clock = enums.Clocks.BACKTEST
# exchange = enums.Exchanges.REAL
# strat_patience = 15 * 60

# Backtest
tick_interval_s = 60
clock = enums.Clocks.BACKTEST
exchange = enums.Exchanges.BACKTEST
strat_patience_s = 60 * 60 * 8

# Clock
start_time_dt = datetime.datetime(2021, 5, 1, 0, 0) 
end_time_dt = datetime.datetime(2021, 7, 1, 0, 0)

# Logger
logger = enums.Logger.CONSOLE
results_path = 'results_{}.pkl'.format(datetime.datetime.now().strftime("%H-%M - %d%m"))
results_folder = 'results'
log_result_ticks = 60 * 24

# Strategy
strat = enums.Strats.ONLYLONG

# Simple strategy
strat_profit_target = 0.01
strat_tolerance = 0.005
stop_loss = 0.1
trade_amount_currency = 500
fpr_treshold = 0.99

# Data
granularity = '1min'
granularity_models = '30min'
data_path = 'data'
update_data = True

# Exchange
symbol = 'BTCUSDT'
balance_asset = 0.1
balance_currency = 1000
fee = 0.00075
asset = 'BTC'
currency = 'USDT'
api_secret = "P3vgV6FlaPgXZFly0fJGLUlnc5Z3GpQ7EI36plfXDHMKvji8NNSjhTtQ8INuTn5g"
api_key = "pPyMG5laPPU1Y4zJIRe0BdjKakhZv0Js9r8Nl1zQGX2UD1Wboe2eoRpD9e2wIowE "

# Predictor
models_path = "models"

# Telegram
telegram_token = "1484944537:AAGwrO3A0dLn9nJ3iPqaWXFlPmm9MAHwylU"
telegram_chat_id = "@BTCBaldBot_UY"

granularity_s = time_utils.granularityStrToSeconds(granularity)
granularity_models_s = time_utils.granularityStrToSeconds(granularity_models)

start_time = int(time_utils.dateToEpoc(start_time_dt))
end_time = int(time_utils.dateToEpoc(end_time_dt))